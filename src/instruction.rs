use crate::pager;
use std::convert::Into;

/// Used for tracking the architecture providing the instruction
/// in question
#[derive(Debug)]
pub enum Arch {
    Aarch64,
    Amd64
}

#[derive(Debug)]
pub struct Instruction {
    /// Architecture of the instruction
    arch: Arch,
    /// Instruction mnemonic
    name: String,
    /// Instruction description
    description: String
}

impl Instruction {
    pub fn new(arch: Arch, name: &str, description: &str) -> Instruction {
        Instruction {
            arch,
            name: name.into(),
            description: description.into()
        }
    }
}

impl From<Arch> for String {
    fn from(arch: Arch) -> Self {
        match arch {
            Arch::Aarch64 => "aarch64",
            Arch::Amd64 => "amd64"
        }.into()
    }
}

impl pager::Pagerize for Instruction {
    fn pagerize(self) -> pager::Content {
        let heading = format!("{}({})", self.name.to_uppercase(), String::from(self.arch));
        pager::Content::new(&heading, &self.name, &self.description)
    }
}
