#![warn(
    rust_2018_idioms,
    missing_debug_implementations,
    rustdoc::broken_intra_doc_links
)]

/// Instruction abstractions
pub mod instruction;

/// Pager displaying instruction info
pub mod pager;
