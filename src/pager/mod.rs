use std::fmt;

use crossterm::terminal;

/// Interface the user interacts with
pub mod tui;

const INDENT_WIDTH: u16 = 8;

#[derive(Debug)]
pub struct Content {
    /// Heading of the page to be displayed, c.f.
    /// CARGO(1)  [...]  General Commands Manual  [...] CARGO(1)
    /// in man cargo
    heading: String,
    /// Name of the page to be displayed under NAME
    name: String,
    /// Words to be displayed in the DESCRIPTION section
    description: Vec<String>
}

impl Content {
    pub fn new(heading: &str, name: &str, description: &str) -> Content {
        Content {
            heading: heading.to_string(),
            name: name.to_string(),
            description: description.split_whitespace()
                                    .map(|s| s.to_string())
                                    .collect::<Vec<String>>()
        }
    }
}

pub trait Pagerize {
    fn pagerize(self) -> Content;
}

impl fmt::Display for Content {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let indent = format!("{: ^1$}", "", INDENT_WIDTH as usize);
        let cols = match terminal::size() {
            Ok((cols, _)) => cols,
            Err(msg) => {
                eprintln!("{}", format!("Could not get terminal size: {}", msg));
                80
            }
        };

        write!(f, "{}\n\n", &self.heading)?;
        write!(f, "NAME\n{}{}\n\n", &indent, &self.name)?;

        write!(f, "DESCRIPTION\n")?;
        let lim = (cols as f32 * 0.75) as u16 - INDENT_WIDTH;

        let mut line: Vec<&str> = Vec::with_capacity(32);
        let mut len = 0usize;

        for word in &self.description {
            if len + word.len() > lim.into() {
                write!(f, "{}{}\n", indent, line.join(" "))?;
                line.clear();
                len = 0;
            }
            line.push(&word);
            len += word.len() + 1;
        }

        if line.len() != 0 {
            write!(f, "{}{}", indent, line.join(" "))?;
        }
        Ok(())
    }
}
