use crate::pager;

use crossterm::{event, execute, terminal};
use std::{error, io, ops};

#[derive(Debug)]
pub struct Tui  {
    /// Number of line showed at top of the window
    line: u64,
    /// Content to be displayed
    content: pager::Content,
    /// Indices into content's description Vec, for formatting
    ranges: Vec<ops::Range<usize>>
}

impl Tui {
    pub fn new(content: pager::Content) -> Tui {
        let ranges = match Tui::partition_description(&content, None) {
            Ok(ranges) => ranges,
            Err(what) => panic!("{}", what)
        };
        Tui { line: 1, content, ranges }
    }

    pub fn run(&mut self) -> Result<(), Box<dyn error::Error>> {
        terminal::enable_raw_mode()?;

        let mut stdout = io::stdout();
        execute!(stdout, event::EnableMouseCapture)?;

        loop {
            let ev = event::read()?;

            match ev {
                event::Event::Key(ev) => {
                    if let Ok(should_exit) = self.key_input(ev) {
                        if should_exit {
                            break;
                        }
                    }
                },
                event::Event::Resize(width, _) => self.resize(width)?,
                _ => ()
            };
        }

        execute!(stdout, event::DisableMouseCapture)?;
        terminal::disable_raw_mode()?;
        Ok(())
    }

    fn key_input(&mut self, ev: event::KeyEvent) -> Result<bool, Box<dyn error::Error>> {
        match ev.code {
            event::KeyCode::Char('j') => self.down(),
            event::KeyCode::Char('k') => self.up(),
            event::KeyCode::Esc => return Ok(true),
            _ => ()
        };

        Ok(false)
    }

    fn partition_description(content: &pager::Content, width: Option<u16>) -> Result<Vec<ops::Range<usize>>, Box<dyn error::Error>> {
        let cols = match width {
            Some(width) => width,
            None => match terminal::size() {
                Ok((cols, _)) => cols,
                Err(msg) => return Err(format!("Could not get terminal size: {}", msg).into())
            }
        };

        let lim = (cols as f32 * 0.75) as u16 - pager::INDENT_WIDTH;

        let mut ranges: Vec<ops::Range<usize>> = Vec::with_capacity(32);
        let mut start = 0usize;
        let mut len = 0usize;

        for (i, word) in content.description.iter().enumerate() {
            if len + word.len() > lim.into() {
                ranges.push(start .. i);
                start = i;
                len = 0;
            }
            len += word.len() + 1;
        }

        if len != 0 {
            ranges.push(start .. content.description.len());
        }

        Ok(ranges)
    }

    fn down(&mut self) {
        // TODO calculate total content size
        self.line += 1u64;
        self.redraw();
    }

    fn up(&mut self) {
        if self.line == 1 {
            return;
        }

        self.line -= 1;
        self.redraw();
    }

    fn resize(&mut self, width: u16) -> Result<(), Box<dyn error::Error>> {
        self.ranges = Tui::partition_description(&self.content, Some(width))?;
        Ok(())
    }

    fn redraw(&self) {

    }
}
