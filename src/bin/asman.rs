use asman::instruction;
use asman::pager::Pagerize;
use asman::pager::tui;

use std::process;

fn main() {
    let desc = "loead asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf load asdf";
    let ins = instruction::Instruction::new(instruction::Arch::Aarch64, "ldr", desc).pagerize();

    let mut tui = tui::Tui::new(ins);
    if let Err(what) = tui.run() {
        eprintln!("{}", what);
        process::exit(1);
    }
}
