FROM archlinux:latest
LABEL maintainer="vilhelm.engstrom@tuta.io"

COPY . /asman
WORKDIR /asman

RUN useradd -m builder                      &&  \
    pacman -Syu --noconfirm --needed rust   && \
    chown -R builder:builder /asman

USER builder
